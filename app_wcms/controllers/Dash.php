<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include 'Controller.php';

class Dash extends Controller {
	
	private $main = "main";
	
	public function __construct()
	{
		parent::__construct();
	}
		
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin())
		{
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data = array(
				"content_id" => "dash_view.php"
			);
			$this->load->view('layouts/'.$this->main, $data);
		}
		
	}

	public function test()
	{
		//$this->load->model('User_model');
		echo '<pre>';
		print_r(\Model\User_model::all());
		exit();
	}
	
}