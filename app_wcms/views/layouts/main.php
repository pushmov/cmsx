<?php $this->load->view('layouts/head');?>
<?php if($this->uri->segment(2)=='login'){ ?>
	<body class="hold-transition login-page">
<?php } else { ?>
	<body class="hold-transition skin-green sidebar-mini fixed">
<?php } ?>
<div class="wrapper">
	<?php $this->load->view('layouts/header');?>

	<?php $this->load->view('layouts/sidemenu');?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">

	<?php $this->load->view($content_id);?>

	<!-- /.content -->
	</div>
	
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> 2.3.7
		</div>
		<strong>Copyright &copy; <?php echo date('Y'); ?> <a href="http://yoltonagusta.com">Yoltzdesign</a>.</strong> All rights
		reserved.
	</footer>
	
</div>
<?php $this->load->view('layouts/footer');?>