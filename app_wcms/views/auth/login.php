<?php $this->load->view('layouts/head');?>
<div class="container">
  <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo form_open("auth/login", array('class' => 'form-horizontal'));?>
              <div class="box-body">
                <div class="form-group">
                  <?php echo form_label(lang('login_identity_label'), 'identity', array('class' => 'col-sm-2 control-label'));?>
                  <div class="col-sm-10">
                    <?php echo form_input($identity);?>
                  </div>
                </div>
                <div class="form-group">
                  <?php echo form_label(lang('login_password_label'), 'password', array('class' => 'col-sm-2 control-label'));?>
                  <div class="col-sm-10">
                    <?php echo form_input($password);?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> <?php echo lang('login_remember_label');?>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <?php echo form_submit('submit', 'Sign in', array('class' => 'btn btn-info pull-right'));?>
              </div>
              <!-- /.box-footer -->
            <?php echo form_close(); ?>
          </div>
  </div>
<?php $this->load->view('layouts/footer');?>