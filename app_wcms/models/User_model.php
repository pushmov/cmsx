<?php
class User_model extends Base_model {

	protected $table = 'users';
	
	const CREATED_AT = 'created_on';

	public $timestamps = false;

}