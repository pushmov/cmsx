<?php

require_once BASEPATH . '../vendor/autoload.php';
use Illuminate\Database\Eloquent\Model as Eloquent;

class Base_model extends Eloquent {

	public function __construct(){
		parent::__construct();
	}
}